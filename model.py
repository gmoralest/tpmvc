import pickle

#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Person:
    def __init__(self, nom, prenom, telephone='', adresse='', ville=''):
        self.nom = nom
        self.prenom = prenom
        self.telephone = telephone
        self.adresse = adresse
        self.ville = ville

    def get_nom(self):
        return self.nom

    def get_prenom(self):
        return self.prenom

    def get_telephone(self):
        return self.telephone

    def get_adresse(self):
        return self.adresse

    def get_ville(self):
        return self.ville

    def __str__(self):
        return self.get_nom()+self.get_prenom()+self.get_telephone()+self.get_adresse()+self.get_ville()


class Ensemble:
    def __init__(self):
        self.list_person = {}

    def insert_person(self, person):
        if isinstance(person, Person):
            nom = person.get_nom()
            self.list_person[f"{nom}"] = person

    def delete_person(self, person_name):
        # del self.list_person[]
        del self.list_person[person_name]
        print('Deleted: ' + person_name)

    def search_person(self, name):
        for element in self.list_person.keys():
            if name in element:
                return self.list_person.get(name)

    def load_data_model(self):
        with open('persons_list.p', 'rb') as persons_list:
            self.list_person = pickle.load(persons_list)
            print("data loaded")

    def save_data_model(self):
        with open('persons_list.p', 'wb') as persons_list:
            pickle.dump(self.list_person, persons_list)
            print("data saved")

    def __str__(self):
        test = ''
        for element in self.list_person:
            test += element
        return test
