import os.path
from view_obj import View
from model import Ensemble
from model import Person

class Controller():
    def __init__(self):
        self.view = View(self)
        self.model = Ensemble()
        if os.path.exists("persons_list.p"):
            self.view.load_data_view()
        
        
    
    def start_view(self):
        self.view.create_fields()
        self.view.main()
    
    def search(self):
        searched_person = self.model.search_person(self.view.get_value("Nom"))
        searched_person_list = []
        if searched_person is not None:
            searched_person_list.append(searched_person.get_nom())
            searched_person_list.append(searched_person.get_prenom())
            searched_person_list.append(searched_person.get_telephone())
            searched_person_list.append(searched_person.get_adresse())
            searched_person_list.append(searched_person.get_ville())
            self.view.display_search(searched_person_list)
        else:
            print("Record not found")

    def delete(self):
        self.model.delete_person(self.view.get_value("Nom"))
        self.view.clear_fields()
    
    def insert(self):
        person = Person(self.view.get_value("Nom"),
            self.view.get_value("Prenom"),
            self.view.get_value("Telephone"),
            self.view.get_value("Adresse"),
            self.view.get_value("Ville"))

        self.model.insert_person(person)
        self.view.clear_fields()


    def button_press_handle(self, buttonId):
        print("[Controller][button_press_handle] "+ buttonId)
        if buttonId == "Search":
            self.search()
        elif buttonId == "Clear":
            self.view.clear_fields()
        elif buttonId == "Insert":
            self.insert()
        elif buttonId == "Delete":
            self.delete()    
        elif buttonId == "Quit":
            self.view.quit()
        else:
            pass
    
    def load_data_controller(self):
        self.model.load_data_model()

    def save_data_controller(self):
        self.model.save_data_model()


if __name__ == "__main__":
    controller = Controller()
    controller.start_view()