# coding: utf-8

from cProfile import label
import os
from tkinter import Button
from tkinter import Entry
from tkinter import filedialog 
from tkinter import Label
from tkinter import Menu
from tkinter import StringVar
from tkinter import Tk
from tkinter import messagebox
"""
Class View of the project

@author : Olivier CHABROL
"""

class View(Tk):
    def __init__(self, controller):
        super().__init__()
        self.controller = controller
        self.widgets_labs = {}
        self.widgets_entry = {}
        self.widgets_button = {}
        self.entries = ["Nom", "Prenom", "Telephone", "Adresse", "Ville"]
        self.buttons = ["Search", "Insert","Delete" ,"Clear", "Quit"]
        self.modelListFields = []
        self.fileName = None
        self.windows ={}
        self.windows["fenetreResult"] = ...
        self.windows["fenetreErreur"] = ...

    def get_value(self, key):
        return self.widgets_entry[key].get()

    def create_fields(self):
        i, j  = 0, 0

        for idi in self.entries:
            lab = Label(self, text=idi.capitalize())
            self.widgets_labs[idi] = lab
            lab.grid(row=i,column=0)

            var = StringVar()
            entry = Entry(self, text=var)
            self.widgets_entry[idi] = entry
            entry.grid(row=i,column=1, columnspan=5)

            i += 1

        for idi in self.buttons:
            buttonW = Button(self, text = idi, command=(lambda button=idi: self.controller.button_press_handle(button)))
            self.widgets_button[idi] = buttonW
            buttonW.grid(row=i+1,column=j, padx=5)

            j += 1
    
    def clear_fields(self):
        for i in self.widgets_entry:
            self.widgets_entry[i].delete(0,"end")

    def load_data_view(self):
        response_load = messagebox.askokcancel("Load", "Would yo like to load your session")
        if response_load:
            self.controller.load_data_controller()
    
    def display_search(self,searched_person):
        self.clear_fields()
        j = 0
        for i in self.widgets_entry:
            self.widgets_entry[i].insert("end",searched_person[j])
            j +=1
        


    def quit(self):
        response1 = messagebox.askokcancel("Quitter", "Are you sure you want to QUIT?")
        if response1:
            response2 = messagebox.askokcancel("Save", "Would you like to SAVE before QUIT the program?")
            if response2:
                self.controller.save_data_controller()
            self.controller.view.destroy()

    def main(self):
        print("[View] main")
        self.title("Annuaire")
        self.mainloop()